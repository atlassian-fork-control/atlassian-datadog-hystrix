Contains a Hystrix Metrics Plugin for sending metrics to Datadog.

**Important!** At the moment the Atlassian Gostatsd server does not support `Histogram`,
 so using `ExecutionTime` instead.
 
# Versioning #
Following SemVer.

# CI Server #
See [this project](https://ecosystem-bamboo.internal.atlassian.com/browse/DATADOG).
 
# Issues #
Use [this](./issues).