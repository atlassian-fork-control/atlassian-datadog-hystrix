<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>com.atlassian.pom</groupId>
    <artifactId>public-pom</artifactId>
    <version>4.0.23</version>
  </parent>

  <groupId>com.atlassian.datadog</groupId>
  <artifactId>atlassian-datadog-hystrix</artifactId>
  <version>0.0.3-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>Atlassian Datadog Hystrix</name>
  <url>http://docs.atlassian.com/${project.artifactId}/${project.version}</url>

  <licenses>
    <license>
      <name>Apache 2</name>
      <distribution>repo</distribution>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>

  <issueManagement>
    <system>Jira</system>
    <url>https://bitbucket.org/atlassian/atlassian-datadog-hystrix/issues</url>
  </issueManagement>

  <scm>
    <connection>scm:git:git@bitbucket.org:atlassian/atlassian-datadog-hystrix.git</connection>
    <developerConnection>scm:git:git@bitbucket.org:atlassian/atlassian-datadog-hystrix.git</developerConnection>
    <url>https://bitbucket.org/atlassian/atlassian-datadog-hystrix</url>
    <tag>HEAD</tag>
  </scm>

  <properties>
    <!--
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    -->
    <atlassian.platform.version>3.0.12</atlassian.platform.version>
    <dogstatsd.client.version>2.2</dogstatsd.client.version>
    <hystrix.version>1.5.8</hystrix.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <!-- External dependencies, using the Platform POMs https://bitbucket.org/atlassian/platform-poms -->
      <dependency>
        <groupId>com.atlassian.platform</groupId>
        <artifactId>platform</artifactId>
        <version>${atlassian.platform.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>com.atlassian.platform</groupId>
        <artifactId>third-party</artifactId>
        <version>${atlassian.platform.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <dependency>
        <groupId>com.datadoghq</groupId>
        <artifactId>java-dogstatsd-client</artifactId>
        <version>${dogstatsd.client.version}</version>
      </dependency>

      <dependency>
        <groupId>com.netflix.hystrix</groupId>
        <artifactId>hystrix-core</artifactId>
        <version>${hystrix.version}</version>
        <exclusions>
          <exclusion>
            <!-- jcl-over-slf4j instead -->
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>com.datadoghq</groupId>
      <artifactId>java-dogstatsd-client</artifactId>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>com.netflix.hystrix</groupId>
      <artifactId>hystrix-core</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.atlassian.annotations</groupId>
      <artifactId>atlassian-annotations</artifactId>
    </dependency>
    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>jsr305</artifactId>
    </dependency>

    <!-- test -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-all</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.hamcrest</groupId>
      <artifactId>hamcrest-library</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-simple</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
      <version>1.2</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <distributionManagement>
    <site>
      <id>atlassian-documentation</id>
      <url>
        scpexe://docs-app.internal.atlassian.com/var/www/domains/docs.atlassian.com/${project.artifactId}/${project.version}
      </url>
    </site>
  </distributionManagement>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
      </plugin>
    </plugins>
  </reporting>

  <ciManagement>
    <system>Bamboo</system>
    <url>https://ecosystem-bamboo.internal.atlassian.com/browse/DATADOG</url>
  </ciManagement>

</project>
