/**
 * Contains Datadog reporter for Hystrix.
 *
 * @since 1.0.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@FieldsAreNonnullByDefault
package com.atlassian.datadog.hystrix;

import com.atlassian.annotations.nonnull.FieldsAreNonnullByDefault;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
