package com.atlassian.datadog.hystrix;

import com.netflix.hystrix.HystrixThreadPoolKey;
import com.netflix.hystrix.HystrixThreadPoolMetrics;
import com.netflix.hystrix.metric.HystrixCommandCompletion;
import com.netflix.hystrix.metric.HystrixThreadPoolCompletionStream;
import com.netflix.hystrix.strategy.metrics.HystrixMetricsPublisherThreadPool;
import com.timgroup.statsd.StatsDClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

class HystrixDatadogMetricsPublisherThreadPool implements HystrixMetricsPublisherThreadPool {
    private static final Logger log = LoggerFactory.getLogger(HystrixDatadogMetricsPublisherThreadPool.class);

    private final HystrixThreadPoolKey threadPoolKey;
    private final HystrixThreadPoolMetrics metrics;
    private final StatsDClient client;

    HystrixDatadogMetricsPublisherThreadPool(HystrixThreadPoolKey threadPoolKey,
                                             HystrixThreadPoolMetrics metrics,
                                             StatsDClient client) {
        this.threadPoolKey = requireNonNull(threadPoolKey);
        this.metrics = requireNonNull(metrics);
        this.client = requireNonNull(client);
    }

    @Override
    public void initialize() {
        log.debug("initialize() called for {}", threadPoolKey.name());

        HystrixThreadPoolCompletionStream.getInstance(threadPoolKey)
            .observe()
            .onBackpressureDrop()
            .subscribe(this::handle);
    }

    @SuppressWarnings("unused")
    private void handle(HystrixCommandCompletion ignored) {
        // HystrixCommandCompletion are recorded using the HystrixDatadogMetricsPublisherCommand
        // so only focus on HystrixThreadPoolMetrics.

        client.recordGaugeValue(
            "hystrix.threadPool.active.count",
            metrics.getCurrentActiveCount().longValue(),
            "threadPoolKey:" + threadPoolKey.name());

        client.recordGaugeValue(
            "hystrix.threadPool.pool.size",
            metrics.getCurrentPoolSize().longValue(),
            "threadPoolKey:" + threadPoolKey.name());

        client.recordGaugeValue(
            "hystrix.threadPool.queue.size",
            metrics.getCurrentQueueSize().longValue(),
            "threadPoolKey:" + threadPoolKey.name());
    }
}
